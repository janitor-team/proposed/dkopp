# dkopp make file

# defaults for parameters that may be pre-defined
CXXFLAGS += -Wall -g -rdynamic -O2 -Wno-format-truncation -Wno-stringop-truncation

PKG_CONFIG ?= pkg-config

CFLAGS = $(CXXFLAGS) $(CPPFLAGS) -c                \
   `$(PKG_CONFIG) --cflags gtk+-3.0`               \
   -I/usr/include/clutter-1.0/                     \
   -I/usr/include/cogl/                            \
   -I/usr/include/json-glib-1.0/                   \
   -I/usr/include/clutter-gtk-1.0/

LIBS = `pkg-config --libs gtk+-3.0` -lpthread -lclutter-1.0 -lclutter-gtk-1.0

dkopp: dkopp.o zfuncs.o
	$(CXX) $(LDFLAGS) -o dkopp dkopp.o zfuncs.o $(LIBS)  \

dkopp.o: dkopp.cc
	$(CXX) $(CFLAGS) -o dkopp.o dkopp.cc  \

zfuncs.o: zfuncs.cc zfuncs.h
	$(CXX) $(CFLAGS) zfuncs.cc  \

# target install directories
PREFIX ?= /usr
BINDIR = $(PREFIX)/bin
DOCDIR = $(PREFIX)/share/doc/dkopp
MANDIR = $(PREFIX)/share/man/man1
MENUDIR = $(PREFIX)/share/applications
SHAREDIR = $(PREFIX)/share/dkopp
DATADIR = $(SHAREDIR)/data
IMAGEDIR = $(SHAREDIR)/images
ICONDIR = $(SHAREDIR)/icons

install: dkopp uninstall
	mkdir -p  $(DESTDIR)$(BINDIR)
	mkdir -p  $(DESTDIR)$(DOCDIR)
	mkdir -p  $(DESTDIR)$(MANDIR)
	mkdir -p  $(DESTDIR)$(MENUDIR) 
	mkdir -p  $(DESTDIR)$(DATADIR)
	mkdir -p  $(DESTDIR)$(IMAGEDIR)
	mkdir -p  $(DESTDIR)$(ICONDIR)
	cp -f  dkopp $(DESTDIR)$(BINDIR)
	cp -f -R  doc/* $(DESTDIR)$(DOCDIR)
	cp -f -R  data/* $(DESTDIR)$(DATADIR)
	cp -f -R  images/* $(DESTDIR)$(IMAGEDIR)
	# man page
	gzip -fk -9 doc/dkopp.man
	mv -f doc/dkopp.man.gz $(DESTDIR)$(MANDIR)/dkopp.1.gz
	# desktop file and icon
	cp -f dkopp.desktop $(DESTDIR)$(MENUDIR)
	cp -f dkopp.png $(DESTDIR)$(ICONDIR)

uninstall:
	rm -f  $(DESTDIR)$(BINDIR)/dkopp
	rm -f -R  $(DESTDIR)$(DOCDIR)
	rm -f  $(DESTDIR)$(MANDIR)/dkopp.*
	rm -f  $(DESTDIR)$(MENUDIR)/dkopp.*
	rm -f -R  $(DESTDIR)$(SHAREDIR)

clean: 
	rm -f  dkopp
	rm -f  *.o
 

